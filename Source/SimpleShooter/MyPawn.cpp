#include "MyPawn.h"

AMyPawn::AMyPawn(){
	PrimaryActorTick.bCanEverTick = false;
}

void AMyPawn::BeginPlay(){
	Super::BeginPlay();
}

void AMyPawn::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent){
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

