#include "BTService_PlayerLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

UBTService_PlayerLocation::UBTService_PlayerLocation() {
	NodeName = TEXT("UpdatePlayerLocation");
}

void UBTService_PlayerLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) {
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (!PlayerPawn) {
		UE_LOG(LogTemp, Warning, TEXT("UBTService_PlayerLocation::TickNode \nPlayerPawn missing!"))
		return;
	}

	OwnerComp.GetBlackboardComponent()->
		SetValueAsVector(GetSelectedBlackboardKey(), PlayerPawn->GetActorLocation());
}
