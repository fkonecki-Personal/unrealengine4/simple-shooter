#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"

UCLASS()
class SIMPLESHOOTER_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void GameHasEnded(AActor* EndGameFocus = nullptr, bool bIsWinner = false);
	
protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
		float RestatrDelay = 5.f;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> HUDClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> LoseScreenClasss;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> WinScreenClasss;
	UPROPERTY(EditAnywhere)
		UUserWidget* HUD;
	FTimerHandle RestartTimer;

};
