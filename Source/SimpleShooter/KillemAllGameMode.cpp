#include "KillemAllGameMode.h"
#include "EngineUtils.h"
#include "ShooterAIController.h"

void AKillemAllGameMode::PawnKilled(APawn* PawnKilled) {
	Super::PawnKilled(PawnKilled);

	// Check did player die
	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
	if (PlayerController) {
		EndGame(false);
	} 

	// Are there any enemies left?
	TActorRange<AShooterAIController> AIControllersInWorld = TActorRange<AShooterAIController>(GetWorld());
	for (AShooterAIController* Controller : AIControllersInWorld) {
		if (!Controller->IsDead()) 
			return;
	}

	// All enemies are dead
	EndGame(true);
}

void AKillemAllGameMode::EndGame(bool bIsPlayerWinner) {
	TActorRange<AController> ControllersInWorld = TActorRange<AController>(GetWorld());

	// Who is the last character alive?
	for (AController* Controller : ControllersInWorld) {
		bool bIsWinner = bIsPlayerWinner == Controller->IsPlayerController();
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}
